package com.utils;

public class TimeCounter {
	private static TimeCounter timer = new TimeCounter();
	private long countedTime;
	private String counterName;
	
	private TimeCounter(){
	}
	private TimeCounter(String timerName){
		this.counterName=timerName;
	}
	
	public static TimeCounter getTimeCounter(){
		return TimeCounter.timer;
	}
	public static TimeCounter getNonStaticTimeCounter(String name){
		return new TimeCounter(name);
	}
	
	public void startCounting(){
		this.countedTime=System.currentTimeMillis();
	}

	public void stopCounting(){
		this.countedTime=System.currentTimeMillis()-this.countedTime;
	}
	
	public long showTime(){
		long tmpTime=System.currentTimeMillis()-this.countedTime;
		return tmpTime;
	}
	//returns time in miliseconds
	public long getTime(){
		return this.countedTime;
	}
	
	public static void main(String[] args) {
		TimeCounter.getTimeCounter().startCounting();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("tmp time: "+TimeCounter.getTimeCounter().showTime());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TimeCounter.getTimeCounter().stopCounting();
		long time =TimeCounter.getTimeCounter().getTime();
		System.out.println("end time: "+time);
		if(time>=2000 && time<2010)
			System.out.println("awesome");
	}
	public String getCounterName(){
		return this.counterName;
	}
}
