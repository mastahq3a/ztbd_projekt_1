package com.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.jezhumble.javasysmon.JavaSysMon;
import com.jezhumble.javasysmon.MemoryStats;

public class ProcessMonitor {
	private JavaSysMon jsm;
	private static Runtime r = Runtime.getRuntime();
	private String osName= jsm.osName().toLowerCase();
	private Map<String,Integer> memUsage = new HashMap<String,Integer>();
	private static ProcessMonitor processMonitor = new ProcessMonitor();
	
	private ProcessMonitor(){
	}
	public static ProcessMonitor getInstance(){
		return processMonitor;
	}
	public Map<String,String> getSystemInfo(){
		jsm = new JavaSysMon();
		Map<String,String> sysInfo = new HashMap<String,String>();
		sysInfo.put("System name",this.osName);
		sysInfo.put("Process id", String.valueOf(this.jsm.currentPid()));
		sysInfo.put("Thread id", String.valueOf(Thread.currentThread().getId()));
		sysInfo.put("Cpu frequency", String.valueOf(this.jsm.cpuFrequencyInHz()));
		sysInfo.put("Number of cpus", String.valueOf(this.jsm.numCpus()));
		MemoryStats memStat = this.jsm.physical();
		sysInfo.put("Total physical memory in bytes", String.valueOf(memStat.getTotalBytes()));
		sysInfo.put("Free physical memory in bytes", String.valueOf(memStat.getFreeBytes()));
		sysInfo.put("Used physical memory in bytes", String.valueOf(memStat.getTotalBytes()-memStat.getFreeBytes()));
		MemoryStats opMemStat = this.jsm.swap();
		sysInfo.put("Total swap memory in bytes", String.valueOf(opMemStat.getTotalBytes()));
		sysInfo.put("Free swap memory in bytes", String.valueOf(opMemStat.getFreeBytes()));
		sysInfo.put("Used swap memory in bytes", String.valueOf(opMemStat.getTotalBytes()-memStat.getFreeBytes()));
		return sysInfo;
	}
	public List<String> getProcessList(){
		Process p;
		try {
			if(osName.contains("windows")){
				p = r.exec("tasklist");
			}else{
				p = r.exec("ps aux");
			}
			//p.waitFor();
			BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line="";
			ArrayList<String> processInfo = new ArrayList<String>(); 
			while((line =b.readLine())!=null){
				processInfo.add(line);
			}
			p.destroyForcibly();
			b.close();
			return processInfo;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String memoryUsage(int pid){
		Process p;
		if(osName.contains("windows")){
			try {
				p = r.exec("tasklist /FI \"PID eq "+pid+"\"");
				BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
				ArrayList<String> out = new ArrayList<String>();
				String line =null;
				String memUsage=null;
				while((line=b.readLine())!=null){
					line = line.replaceAll("\\s", " ").toLowerCase();
					if(line.contains(String.valueOf(pid))){
						String[] cols = line.split(" ");
						memUsage = cols[4];
					}
				}
				p.destroyForcibly();
				b.close();
				return memUsage;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "COULDN'T READ MEMORY USAGE";
		}else{
			return "SYSTEM NOT SUPPORTED";
			//throw new UnsupportedOperationException();
		}
	}
	public JavaSysMon getJavaSysMon(){
		return new JavaSysMon();
	}
	
	public static void main(String[] args){
		ProcessMonitor pm = new ProcessMonitor();
		for(String s : pm.getProcessList()){
			System.out.println(s);
		}
	}
}
