package com.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonMapper{
	private JSONObject jm;
	private String tempKey;
	public JsonMapper(JSONObject jm){
		this.jm = jm;
	}
	public JsonMapper(){
		this.jm = new JSONObject();
	}
	public class Protector{ //to protect calling value method without setting up a key
		public boolean value(String value) throws JSONException{
			if(tempKey==null || tempKey.equals(""))
				return false;
			jm.put(tempKey,value);
			tempKey="";
			return true;
		}
	}
	public Protector key(String key){
		tempKey = key;
		return new Protector();
	}
	public JSONObject getJSONObject(){
		return this.jm;
	}
	public JsonMapper setJSONObject(JSONObject obj){
		this.jm = obj;
		return this;
	}
}

