package com.main;

import java.util.List;

import com.jezhumble.javasysmon.JavaSysMon;
import com.jezhumble.javasysmon.OsProcess;

public class LocalTest {
	private static JavaSysMon javasysmon = new JavaSysMon();
	public static void main(String[] args) {
		System.out.print(javasysmon.osName());
		System.out.print("\n"+javasysmon.currentPid());
		System.out.print("\n"+javasysmon.cpuFrequencyInHz());
		System.out.print("\n"+javasysmon.numCpus());
		System.out.print("\n"+javasysmon.uptimeInSeconds());
		System.out.print("\n"+javasysmon.cpuTimes().getUserMillis());
		System.out.print("\n"+javasysmon.physical().toString());
		System.out.print("\n"+javasysmon.swap().toString());
		System.out.print("<br>Process tree:<br/>");
		@SuppressWarnings("unchecked")
		List<OsProcess> childProcesses = javasysmon.processTree().children();
		for(OsProcess proc : childProcesses){
			System.out.print(proc.processInfo().toString()+"\n");
			if(proc.children()!=null){
				@SuppressWarnings("unchecked")
				List<OsProcess> proc1 = proc.children();
				for(OsProcess proc2 : proc1){
					System.out.print("   "+proc2.processInfo().toString()+"\n");
					if(proc2.children()!=null){
						@SuppressWarnings("unchecked")
						List<OsProcess> proc3 = proc2.children();
						for(OsProcess proc4 : proc3){
							System.out.print("    "+proc4.processInfo().toString()+"\n");
						}
					}
				}
			}
		}
	}

}
