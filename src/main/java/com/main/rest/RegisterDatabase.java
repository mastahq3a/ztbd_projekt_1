package com.main.rest;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.hibernate.Query;
import org.hibernate.Session;

import com.database.entities.DatabaseEntity;
import com.main.LocalConfigurator;

@Path("/register")
public class RegisterDatabase {
	EntityManagerFactory emf;
	EntityManager em;
	
	@GET
	@Path("/{param}")
	public String registerDatabaseGET(@PathParam("param") String dbName){
		String id="-1";
		if(Arrays.asList(LocalConfigurator.dbNames).contains(dbName)){
			emf=Persistence.createEntityManagerFactory("ztbd.first");
			em = emf.createEntityManager();
			Session ses = em.unwrap(Session.class);
			Query q = ses.createQuery("from DatabaseEntity where name = :name");
			q.setParameter("name", dbName);
			DatabaseEntity dbs  = (DatabaseEntity) q.list().get(0);
			return new String(dbs.getId()+"");
		}
		
		return id;
	}
	@POST
	@Path("/{param}")
	public Response registerDatabasePOST(@PathParam("param") String dbName){
		boolean out=false;
		if(Arrays.asList(LocalConfigurator.dbNames).contains(dbName)){
			out=true;
		}
		
		return Response.status(200).entity(out).build();
	}
}
