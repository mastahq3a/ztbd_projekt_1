package com.main.rest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.database.connections.MySQL;
import com.database.entities.DatabaseEntity;
import com.database.entities.TestResultEntity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.utils.TimeCounter;
import com.utils.Utils;

@Path("/")
public class DatabaseTestResults {
	EntityManagerFactory emf;
	EntityManager em;
	@POST
	@Path("/send/result/{param}")
	public Response postTestResults(@PathParam("param") String dbId, String json){
		JsonParser parser = new JsonParser();
		JsonObject jo = parser.parse(json).getAsJsonObject();
		JsonObject create = jo.getAsJsonObject("create");
		JsonObject read = jo.getAsJsonObject("read");
		JsonObject update = jo.getAsJsonObject("update");
		JsonObject delete = jo.getAsJsonObject("delete");
		Gson gson = new Gson();
		try{
			TestResultEntity treC = gson.fromJson(create, TestResultEntity.class);
			TestResultEntity treR = gson.fromJson(read, TestResultEntity.class);
			TestResultEntity treU = gson.fromJson(update, TestResultEntity.class);
			TestResultEntity treD = gson.fromJson(delete, TestResultEntity.class);
			emf = Persistence.createEntityManagerFactory("ztbd.first");
			em= emf.createEntityManager();
			Session ses = em.unwrap(Session.class);
			Query query = ses.createQuery("from DatabaseEntity where id = :id");
			query.setParameter("id", Integer.valueOf(dbId));
			DatabaseEntity dbe = (DatabaseEntity)query.list().get(0);
			treC.setDataBase(dbe);treR.setDataBase(dbe);
			treU.setDataBase(dbe);treD.setDataBase(dbe);
			em.getTransaction().begin();
			em.persist(treC);
			em.persist(treR);
			em.persist(treU);
			em.persist(treD);
			em.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).entity("Internal server error").build();
		}
		return Response.status(201).entity("Test results accepted.").build();
	}
	
	@GET
	@Path("/list/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getTestResults(@PathParam("param") String db){
		emf=Persistence.createEntityManagerFactory("ztbd.first");
		em = emf.createEntityManager();
		Session ses = em.unwrap(Session.class);
		Query query = ses.createQuery("select t from TestResultEntity t where t.dataBase.id = :id");
		query.setParameter("id", Integer.valueOf(db));
		List<TestResultEntity> treL = query.list();
		JSONArray ja = new JSONArray();
		for(TestResultEntity t : treL){
			JSONObject jo = new JSONObject();
			jo.put("id", t.getId());
			jo.put("records", t.getHits());
			jo.put("time", t.getTime());
			jo.put("test_type", t.getJobName());
			jo.put("dbName", t.getDatabasename());
			jo.put("dbId", t.getDataBase().getId());
			jo.put("cpu", Utils.round(t.getCpu(), 2)*100);
			jo.put("memory",Utils.round(t.getMemory()/1024.0/1024.0, 2));
			ja.put(jo); //returns jajo
		}
		em.close();
		emf.close();
		return ja.toString();
	}
	@GET
	@Path("/avg/{db1}/{db2}/{job}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAvgResultForDb(@PathParam("db1") String db1, 
									@PathParam("db2") String db2, 
									@PathParam("job") String job) throws SQLException{
		
		MySQL my = MySQL.getInstance();
		Connection con = my.getConnection();
		ResultSet hiRs = con.createStatement()
				.executeQuery("select distinct hits from `test_result`");
		//mam hitsy
		List<Integer> hitZ = new ArrayList<>();
		while(hiRs.next()){
			hitZ.add(hiRs.getInt(1));
		}
		Map<Integer,String> dbS = new HashMap<>();
		ResultSet dbRs = con.createStatement().executeQuery("SELECT id,name from database_entity");
		while(dbRs.next())
			dbS.put(dbRs.getInt(1), dbRs.getString(2));
		
		JSONArray rec = new JSONArray();
		JSONArray db1t = new JSONArray();
		JSONArray db2t = new JSONArray();
		JSONArray db1c = new JSONArray();
		JSONArray db2c = new JSONArray();
		JSONArray db1m = new JSONArray();
		JSONArray db2m = new JSONArray();
		for(Integer hi : hitZ){
			rec.put(hi);
			ResultSet rdb1 = con.createStatement()
					.executeQuery("select name,AVG(time),AVG(cpu),AVG(memory) from test_result "
						+ "join database_entity on database_entity.id=test_result.db_id "
						+ "where db_id='"+Integer.valueOf(db1)+"' and hits="+hi+" "
						+ "and job_name='"+job+"';");
			ResultSet rdb2 = con.createStatement()
					.executeQuery("select name,AVG(time),AVG(cpu),AVG(memory) from test_result "
						+ "join database_entity on database_entity.id=test_result.db_id "
						+ "where db_id='"+Integer.valueOf(db2)+"' and hits="+hi+" "
						+ "and job_name='"+job+"';");
			
			rdb1.next();
			rdb2.next();
			db1t.put(Utils.round(rdb1.getDouble(2), 2));
			db2t.put(Utils.round(rdb2.getDouble(2), 2));
			db1c.put(Utils.round(rdb1.getDouble(3)*100, 2));
			db2c.put(Utils.round(rdb2.getDouble(3)*100, 2));
			db1m.put(Utils.round(rdb1.getDouble(4)/1024.0/1024.0, 2));
			db2m.put(Utils.round(rdb2.getDouble(4)/1024.0/1024.0, 2));
		}
		JSONObject dbT = new JSONObject();
		dbT.put(dbS.get(Integer.valueOf(db1)), db1t);
		dbT.put(dbS.get(Integer.valueOf(db2)), db2t);
		JSONObject dbC = new JSONObject();
		dbC.put(dbS.get(Integer.valueOf(db1)), db1c);
		dbC.put(dbS.get(Integer.valueOf(db2)), db2c);
		JSONObject dbM = new JSONObject();
		dbM.put(dbS.get(Integer.valueOf(db1)), db1m);
		dbM.put(dbS.get(Integer.valueOf(db2)), db2m);
		
		JSONObject packer = new JSONObject();
		packer.put("test_type", job);
		packer.put("times", dbT);
		packer.put("records", rec);
		packer.put("cpu", dbC);
		packer.put("memory", dbM);
		con.close();
		return packer.toString();
	}
	@GET
	@Path("/avg/name/{db1}/{db2}/{job}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAvgResultForDbNames(@PathParam("db1") String db1, 
									@PathParam("db2") String db2, 
									@PathParam("job") String job) throws SQLException{
		
		MySQL my = MySQL.getInstance();
		TimeCounter.getTimeCounter().startCounting();
		Connection con = my.getConnection();
		ResultSet hiRs = con.createStatement()
				.executeQuery("select distinct hits from `test_result`");
		//mam hitsy
		List<Integer> hitZ = new ArrayList<>();
		while(hiRs.next()){
			hitZ.add(hiRs.getInt(1));
		}
		Map<String,Integer> dbS = new HashMap<>();
		ResultSet dbRs = con.createStatement().executeQuery("SELECT name,id from database_entity");
		while(dbRs.next())
			dbS.put(dbRs.getString(1), dbRs.getInt(2));
		
		JSONArray rec = new JSONArray();
		JSONArray db1t = new JSONArray();
		JSONArray db2t = new JSONArray();
		JSONArray db1c = new JSONArray();
		JSONArray db2c = new JSONArray();
		JSONArray db1m = new JSONArray();
		JSONArray db2m = new JSONArray();
		for(Integer hi : hitZ){
			rec.put(hi);
			ResultSet rdb1 = con.createStatement()
					.executeQuery("select name,AVG(time),AVG(cpu),AVG(memory) from test_result "
						+ "join database_entity on database_entity.id=test_result.db_id "
						+ "where db_id='"+dbS.get(db1)+"' and hits="+hi+" "
						+ "and job_name='"+job+"';");
			ResultSet rdb2 = con.createStatement()
					.executeQuery("select name,AVG(time),AVG(cpu),AVG(memory) from test_result "
						+ "join database_entity on database_entity.id=test_result.db_id "
						+ "where db_id='"+dbS.get(db2)+"' and hits="+hi+" "
						+ "and job_name='"+job+"';");
			
			rdb1.next();
			rdb2.next();
			db1t.put(Utils.round(rdb1.getDouble(2), 2));
			db2t.put(Utils.round(rdb2.getDouble(2), 2));
			db1c.put(Utils.round(rdb1.getDouble(3)*100, 2));
			db2c.put(Utils.round(rdb2.getDouble(3)*100, 2));
			db1m.put(Utils.round(rdb1.getDouble(4)/1024.0/1024.0, 2));
			db2m.put(Utils.round(rdb2.getDouble(4)/1024.0/1024.0, 2));
		}
		TimeCounter.getTimeCounter().stopCounting();
		JSONObject dbT = new JSONObject();
		dbT.put(db1, db1t);
		dbT.put(db2, db2t);
		JSONObject dbC = new JSONObject();
		dbC.put(db1, db1c);
		dbC.put(db2, db2c);
		JSONObject dbM = new JSONObject();
		dbM.put(db1, db1m);
		dbM.put(db2, db2m);
		
		JSONObject packer = new JSONObject();
		packer.put("test_type", job);
		packer.put("times", dbT);
		packer.put("records", rec);
		packer.put("cpu", dbC);
		packer.put("memory", dbM);
		packer.put("response_time", TimeCounter.getTimeCounter().getTime());
		con.close();
		return packer.toString();
	}
	

	@GET
	@Path("/list/avg")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAvgResults() throws SQLException{
		emf=Persistence.createEntityManagerFactory("ztbd.first");
		em = emf.createEntityManager();
		Session ses = em.unwrap(Session.class);
		JSONArray ja = new JSONArray(getNonEmptyDbList());
		List<Integer> dbIds = new ArrayList<>();
		for(Object o : ja){
			JSONObject jo = new JSONObject(o.toString());
			dbIds.add(Integer.valueOf(jo.get("id").toString()));
		}
		MySQL my = MySQL.getInstance();
		Connection con = my.getConnection();
		ResultSet hiRs = con.createStatement()
				.executeQuery("select distinct hits from `test_result`");
		//mam hitsy
		List<Integer> hitZ = new ArrayList<>();
		while(hiRs.next()){
			hitZ.add(hiRs.getInt(1));
		}
		hiRs.close();
		String[] jobs = {"create","read","update","delete"};
		JSONArray jA = new JSONArray();
		for(int i=0;i<ja.length();i++){
			JSONObject ooo = new JSONObject(ja.get(i).toString());
			JSONObject dbO = new JSONObject();
			JSONArray jarr = new JSONArray();
			for(String ss : jobs){
				JSONObject jobO = new JSONObject();
				JSONArray jobA = new JSONArray();
				for(Integer h : hitZ){
					JSONObject hitO = new JSONObject();
					JSONObject resO = new JSONObject();
					ResultSet rrr = con.createStatement()
						.executeQuery("select name,AVG(time),AVG(cpu),AVG(memory) from test_result "
							+ "join database_entity on database_entity.id=test_result.db_id "
							+ "where db_id='"+ooo.getInt("id")+"' and hits="+h.intValue()+" "
							+ "and job_name='"+ss+"';");
					while(rrr.next()){
						resO.put("dbName", rrr.getString(1));
						resO.put("time", Utils.round(rrr.getDouble(2), 2));
						resO.put("cpu", Utils.round(rrr.getDouble(3)*100, 2));
						resO.put("memory", Utils.round(rrr.getDouble(4)/1024.0/1024.0, 2));
					}
					hitO.put(h+"", resO);
					jobA.put(hitO);
				}
				jobO.put(ss,jobA);
				jarr.put(jobO);
			}
			dbO.put(ooo.getString("name"), jarr);
			jA.put(dbO);
		}
		
		con.close();
		
		JSONObject packer = new JSONObject();
		packer.put("database", ja);
		packer.put("result", jA);
		return packer.toString();
	}
	@GET
	@Path("/list/full")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllTestResults(){
		emf=Persistence.createEntityManagerFactory("ztbd.first");
		em = emf.createEntityManager();
		JSONArray ja = new JSONArray(getNonEmptyDbList());
		List<Integer> dbIds = new ArrayList<>();
		for(Object o : ja){
			JSONObject jo = new JSONObject(o.toString());
			dbIds.add(Integer.valueOf(jo.get("id").toString()));
		}
		JSONArray jA = new JSONArray();
		for(Integer i : dbIds){
			JSONObject jo = new JSONObject();
			jo.put(""+i, getTestResults(i));
			jA.put(jo);
		}
		JSONObject packer = new JSONObject();
		packer.put("database", ja);
		packer.put("result", jA);
		
		return packer.toString();
	}
	public JSONArray getTestResults(int db){
		emf=Persistence.createEntityManagerFactory("ztbd.first");
		em = emf.createEntityManager();
		Session ses = em.unwrap(Session.class);
		Query query = ses.createQuery("select t from TestResultEntity t where t.dataBase.id = :id");
		query.setParameter("id", db);
		List<TestResultEntity> treL = query.list();
		JSONArray ja = new JSONArray();
		for(TestResultEntity t : treL){
			JSONObject jo = new JSONObject();
			jo.put("id", t.getId());
			jo.put("records", t.getHits());
			jo.put("time", t.getTime());
			jo.put("test_type", t.getJobName());
			jo.put("dbName", t.getDatabasename());
			jo.put("dbId", t.getDataBase().getId());
			jo.put("cpu", Utils.round(t.getCpu(), 2)*100);
			jo.put("memory",Utils.round(t.getMemory()/1024.0/1024.0, 2));
			ja.put(jo); //returns jajo
		}
		em.close();
		emf.close();
		return ja;
	}
	
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public String getNonEmptyDbList(){
		emf=Persistence.createEntityManagerFactory("ztbd.first");
		em = emf.createEntityManager();
		Session session = em.unwrap(Session.class);
		Query q = session.createQuery("from DatabaseEntity");
		List<DatabaseEntity> dbs = q.list();
		dbs = dbs.stream().filter(dbe ->{
			if(dbe.getTestResults().size()>0)
				return true;
			return false;
		}).collect(Collectors.toList());
		JSONArray ja = new JSONArray();
		for(DatabaseEntity d : dbs){
			JSONObject jo = new JSONObject();
			jo.put("id", d.getId());
			jo.put("name", d.getName());
			ja.put(jo);
		}
		em.close();
		emf.close();
		return ja.toString();
	}
	@GET
	@Path("/list/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDbList(){
		emf=Persistence.createEntityManagerFactory("ztbd.first");
		em = emf.createEntityManager();
		Session session = em.unwrap(Session.class);
		Query q = session.createQuery("from DatabaseEntity");
		List<DatabaseEntity> dbs = q.list();
		JSONArray ja = new JSONArray();
		for(DatabaseEntity d : dbs){
			JSONObject jo = new JSONObject();
			jo.put("id", d.getId());
			jo.put("name", d.getName());
			ja.put(jo);
		}
		em.close();
		emf.close();
		return ja.toString();
	}
	
}
//List<TestResultEntity> tL = new ArrayList<>();
//for(Integer i : dbIds){
//	Query query = ses.createQuery("select t from TestResultEntity t where t.dataBase.id = :id");
//	query.setParameter("id", i);
//	tL.addAll(query.list());
//}
//Map<Integer,List<TestResultEntity>> tLA = new HashMap<>();
//for(TestResultEntity t : tL){
//	if(tLA.containsKey(t.getHits())){
//		tLA.get(t.getHits()).add(t);
//	}else{
//		List<TestResultEntity> la = new ArrayList<>();
//		la.add(t);
//		tLA.put(t.getHits(), la);
//	}
//}
//JSONArray jA = new JSONArray();
//Map<String,List<TestResultEntity>> omg = new HashMap<>();	
//for(Map.Entry<Integer, List<TestResultEntity>> ent : tLA.entrySet()){
//	//mapa zawierająca baze,resulty dla nich i tych samych hitow
//	for(TestResultEntity tt : ent.getValue()){
//		if(omg.containsKey(tt.getDatabasename())){
//			omg.get(tt.getDatabasename()).add(tt);
//		}else{
//			List<TestResultEntity> la = new ArrayList<>();
//			la.add(tt);
//			omg.put(tt.getDatabasename(), la);
//		}
//	}
//}
//for(Map.Entry<String, List<TestResultEntity>> ent : omg.entrySet()){
//	JSONObject joo = new JSONObject();
//	joo.put("dbName",ent.getKey());
//	int iterator=0;
//	double timeA=0,cpuA=0,memA=0;
//	for(TestResultEntity tt : ent.getValue()){
//		
//	}
//	
//}
//
//for(Integer h : hitZ){
//	for(String ss : jobs){
//		for(int i=0;i<ja.length();i++){
//			JSONObject ooo = new JSONObject(ja.get(i).toString());
//			JSONObject toA = new JSONObject();
//			ResultSet rrr = con.createStatement()
//					.executeQuery("select name,AVG(time),AVG(cpu),AVG(memory) from test_result "
//							+ "join database_entity on database_entity.id=test_result.db_id "
//							+ "where db_id='"+ooo.getInt("id")+"' and hits="+h.intValue()+" "
//									+ "and job_name='"+ss+"';");
//			while(rrr.next()){
//				toA.put("dbName", rrr.getString(1));
//				toA.put("time", rrr.getDouble(2));
//				toA.put("cpu", rrr.getDouble(3));
//				toA.put("memory", rrr.getDouble(4));
//			}
//			toA.put("hits", h);
//			toA.put("jobName", ss);
//			jA.put(toA);
//			rrr.close();
//		}
//	}
//}