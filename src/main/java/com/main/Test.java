package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.database.entities.DatabaseEntity;
import com.database.entities.TestResultEntity;
import com.jezhumble.javasysmon.JavaSysMon;
import com.jezhumble.javasysmon.OsProcess;
import com.jobs.JobRunner;

/**
 * Servlet implementation class Main
 */
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private JavaSysMon javasysmon = new JavaSysMon();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameterMap().isEmpty()){
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.write("<html><head><title>kupa</title></head><body>");
			out.write(javasysmon.osName());
			out.write("<br />"+javasysmon.currentPid());
			out.write("<br />"+javasysmon.cpuFrequencyInHz());
			out.write("<br />"+javasysmon.numCpus());
			out.write("<br />"+javasysmon.uptimeInSeconds());
			out.write("<br />"+javasysmon.cpuTimes().getUserMillis());
			out.write("<br />"+javasysmon.physical().toString());
			out.write("<br />"+javasysmon.swap().toString());
			out.write("<br>Process tree:<br/>");
			List<OsProcess> childProcesses = javasysmon.processTree().children();
			for(OsProcess proc : childProcesses){
				out.write(proc.processInfo().toString()+"<br />");
				if(proc.children()!=null){
					List<OsProcess> proc1 = proc.children();
					for(OsProcess proc2 : proc1){
						out.write("&nbsp;&nbsp;&nbsp; "+proc2.processInfo().toString()+"<br />");
						if(proc2.children()!=null){
							List<OsProcess> proc3 = proc2.children();
							for(OsProcess proc4 : proc3){
								out.write("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "+proc4.processInfo().toString()+"<br />");
							}
						}
					}
				}
			}
		}else{
			String resourcePath = request.getRequestURI();
			System.out.println(resourcePath);
			String[] params = resourcePath.split("/");
			List<String> paramList = Arrays.asList(params);
			if(paramList.contains("test")){
				TestResultEntity tre = new TestResultEntity();
				EntityManagerFactory factory = Persistence.createEntityManagerFactory("ztbd.first");
				EntityManager em = factory.createEntityManager();
				int index=0;
				for(int i=0;i<params.length;i++){
					if(params[i].equals("hits")) index=i;
				}
				int hits = Integer.valueOf(params[index+1]);
				JobRunner jr = new JobRunner();
				DatabaseEntity database = em.find(DatabaseEntity.class, 1);
//				jr.addJob(RedisJobs.getRedisCreateJob(hits));
//				jr.run();
//				tre.setDataBase(redisDb);
				
				response.setHeader("Content-type", "application/json");
				//run main jobs, return json, write to mysql
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
