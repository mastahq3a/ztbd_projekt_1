package com.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Query;
import org.hibernate.Session;

import com.database.entities.DatabaseEntity;

public class LocalConfigurator {
	public static String[] dbNames = {"redis","mongodb","mysql",
			"riak","oracle","mssql",
			"postgresql","cassandra","orientdb",
			"couchdb","sqlite","neo4j",
			"dashdb","elasticsearch","hbase",
			"saphana","cloudant","derby","rocksdb"};
	public static void main(String[] args){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("ztbd.first");
		EntityManager em = factory.createEntityManager();
		Session ses = em.unwrap(Session.class);
		Query query = ses.createQuery("from DatabaseEntity");
		@SuppressWarnings("unchecked")
		List<DatabaseEntity> dbs = query.list();
		for(String s : dbNames){
			DatabaseEntity testent = new DatabaseEntity();
			testent.setName(s);
			if(!dbs.contains(testent)){
				em.getTransaction().begin();
				DatabaseEntity dbe = new DatabaseEntity();
				dbe.setName(s);
				em.persist(dbe);
				em.getTransaction().commit();
			}
		}
		em.close();
		factory.close();
	}
}
