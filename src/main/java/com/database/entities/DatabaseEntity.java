package com.database.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="database_entity",
	uniqueConstraints={
		@UniqueConstraint(columnNames={"id","name"})
	}
)
public class DatabaseEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1968171547974732716L;

	public DatabaseEntity(){super();}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",unique=true)
	private Integer id;
	@Column(name="name",unique=true,nullable=false)
	private String name;
	@OneToMany(fetch = FetchType.LAZY, mappedBy="dataBase", cascade=CascadeType.ALL)
	private List<TestResultEntity> testResults;
	
	public List<TestResultEntity> getTestResults() {
		return testResults;
	}
	public void setTestResults(List<TestResultEntity> testResults) {
		this.testResults = testResults;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatabaseEntity other = (DatabaseEntity) obj;
		if(this.getName().equals(other.getName()))
			return true;
		return false;
	}
	
}
