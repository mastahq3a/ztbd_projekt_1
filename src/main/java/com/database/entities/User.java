package com.database.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "users", 
	uniqueConstraints = {
		@UniqueConstraint(columnNames ="login"),
		@UniqueConstraint(columnNames="id")
	}
)
public class User implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 3505769827555959400L;
	public User() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique=true)
    private Long id;
    @Column(name = "login", unique=true, nullable=false, length=100)
    private String login;
    @Column(name = "password", nullable=false, length=100)
    private String password;
    @Column(name="email", nullable=false, length=100)
    private String email;
    @Column(name = "creation_date")
    private Date creationDate;
    @Column(name = "modification_date")
    private Date modificationDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private UserStatus status;
    public String getEmail() {
        return email;
    }
    public User setEmail(String email) {
        this.email = email;
        return this;
    }
    public Long getId() {
        return id;
    }
    public User setId(Long id) {
        this.id = id;
        return this;
    }
    public String getLogin() {
        return login;
    }
    public User setLogin(String login) {
        this.login = login;
        return this;
    }
    public String getPassword() {
        return password;
    }
    public User setPassword(String password) {
        this.password = password;
        return this;
    }
    public Date getCreationDate() {
        return creationDate;
    }
    public User setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }
    public Date getModificationDate() {
        return modificationDate;
    }
    public User setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
        return this;
    }
    public UserStatus getStatus() {
        return status;
    }
    public User setStatus(UserStatus status) {
        this.status = status;
        return this;
    }
    public int getRank(){
	return this.status.getRank();
    }
}

