package com.database.entities;

public enum UserStatus {
    BANNED(1),INACTIVE(2),NEW(3),WARNED(4),ACTIVE(5),MOD(6),ADMIN(7),SUPERADMIN(8);
    private int rank;
    private UserStatus(int rank){
	this.rank=rank;
    }
    public Integer getRank(){
	return this.rank;
    }
    public int compare(UserStatus a1, UserStatus a2){
	if(a1.getRank()>a2.getRank())
	    return 1;
	if(a1.getRank()<a2.getRank())
	    return -1;
	return 0;
    }
    public boolean isHigher(UserStatus status){
	if(status.getRank()<=this.getRank())
	    return true;
	return false;
    }
}
