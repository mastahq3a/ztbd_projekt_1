package com.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="test_result",
		uniqueConstraints={
				@UniqueConstraint(columnNames={"id"})
		}
)
public class TestResultEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2986849257261970856L;
	public TestResultEntity(){super();}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",unique=true)
	private int id;
	@Column(name="hits",nullable=false)
	private int hits;
	@Column(name="time",nullable=false)
	private double time;
	@Column(name="memory",nullable=false)
	private long memory;
	@Column(name="disk",nullable=false)
	private long disk;
	@Column(name="cpu")
	private float cpu;
	@Column(name="job_name")
	private String jobName;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="db_id")
	private DatabaseEntity dataBase;

	public String getJobName() {
		return jobName;
	}

	public TestResultEntity setJobName(String jobName) {
		this.jobName = jobName;
		return this;
	}

	public int getId() {
		return id;
	}
	public TestResultEntity setId(int id) {
		this.id = id;
		return this;
	}

	public int getHits() {
		return hits;
	}

	public TestResultEntity setHits(int hits) {
		this.hits = hits;
		return this;
	}

	public double getTime() {
		return time;
	}

	public TestResultEntity setTime(double time) {
		this.time = time;
		return this;
	}

	public long getMemory() {
		return memory;
	}

	public TestResultEntity setMemory(long memory) {
		this.memory = memory;
		return this;
	}

	public long getDisk() {
		return disk;
	}

	public TestResultEntity setDisk(long disk) {
		this.disk = disk;
		return this;
	}

	public float getCpu() {
		return cpu;
	}

	public TestResultEntity setCpu(float cpu) {
		this.cpu = cpu;
		return this;
	}

	public DatabaseEntity getDataBase() {
		return dataBase;
	}
	public String getDatabasename(){
		return dataBase.getName();
	}
	public TestResultEntity setDataBase(DatabaseEntity dataBase) {
		this.dataBase = dataBase;
		return this;
	}	
}
