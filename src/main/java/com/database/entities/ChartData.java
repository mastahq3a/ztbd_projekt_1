package com.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name="chart_data",
		uniqueConstraints={
				@UniqueConstraint(columnNames={"id"})
		}
)
public class ChartData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7366747600760420878L;
	public ChartData(){super();}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",unique=true)
	private long id;
	@Column(name="test_id",nullable=false)
	private long testId;
	@Column(name="db_id",nullable=false)
	private int dbId;
	@Column(name="db_name",nullable=false)
	private String dbName;
	@Column(name="hits",nullable=false)
	private int hits;
	@Column(name="time",nullable=false)
	private double time;
	@Column(name="memory",nullable=false)
	private long memory;
	@Column(name="disk",nullable=false)
	private long disk;
	@Column(name="cpu")
	private float cpu;
	public long getId() {
		return id;
	}
	public ChartData setId(long id) {
		this.id = id;
		return this;
	}
	public long getTestId() {
		return testId;
	}
	public ChartData setTestId(long testId) {
		this.testId = testId;
		return this;
	}
	public int getDbId() {
		return dbId;
	}
	public ChartData setDbId(int dbId) {
		this.dbId = dbId;
		return this;
	}
	public String getDbName() {
		return dbName;
	}
	public ChartData setDbName(String dbName) {
		this.dbName = dbName;
		return this;
	}
	public int getHits() {
		return hits;
	}
	public ChartData setHits(int hits) {
		this.hits = hits;
		return this;
	}
	public double getTime() {
		return time;
	}
	public ChartData setTime(double time) {
		this.time = time;
		return this;
	}
	public long getMemory() {
		return memory;
	}
	public ChartData setMemory(long memory) {
		this.memory = memory;
		return this;
	}
	public long getDisk() {
		return disk;
	}
	public ChartData setDisk(long disk) {
		this.disk = disk;
		return this;
	}
	public float getCpu() {
		return cpu;
	}
	public ChartData setCpu(float cpu) {
		this.cpu = cpu;
		return this;
	}

	
}
