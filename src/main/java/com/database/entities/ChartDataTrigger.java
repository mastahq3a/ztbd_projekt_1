package com.database.entities;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.*;
import org.hibernate.type.*;

public class ChartDataTrigger extends EmptyInterceptor{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4581150455487768625L;
	private int updates,creates,loads;
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		if ( entity instanceof TestResultEntity ) {
			creates++;
			TestResultEntity tre = (TestResultEntity) entity;
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("ztbddb");
			EntityManager em = factory.createEntityManager();
			Session ses = em.unwrap(Session.class);
			Query db = ses.createQuery("from DatabaseEntity where id = :dbid");
			db.setParameter("dbid", tre.getDataBase().getId());
			DatabaseEntity dbe = (DatabaseEntity) db.list().get(0);
			em.getTransaction().begin();
			ChartData cd = new ChartData();
			cd.setDbName(tre.getDataBase().getName())
			.setDbId(tre.getDataBase().getId())
			.setHits(tre.getHits()).setTime(tre.getTime()).setCpu(tre.getCpu()).setDisk(tre.getDisk())
			.setMemory(tre.getMemory()).setTestId(tre.getId());
			em.persist(cd);
			em.getTransaction().commit();
			em.close();
			factory.close();
		}
		return false;
	}
    public void afterTransactionCompletion(Transaction tx) {
        if ( tx.wasCommitted() ) {
            System.out.println("Creations: " + creates + ", Updates: " + updates + ", Loads: " + loads);
        }
        updates=0;
        creates=0;
        loads=0;
    }
}
