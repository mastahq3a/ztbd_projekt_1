package com.database.connections;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoDB extends AbstractDB{
	private MongoClient mongo = null;
	private MongoDatabase db = null;
	
	public MongoDB(){
		super();
		mongo = new MongoClient(this.connectionProperties.getProperty("mongo_db_url"), Integer.valueOf(connectionProperties.getProperty("mongo_db_port")));
		db = mongo.getDatabase(connectionProperties.getProperty("mongo_db_name"));
		logger.info("Connection to MongoDB initialized.");
	}
	public MongoDatabase getDB(){
		return this.db;
	}
	public MongoClient getMongoClient(){
		return this.mongo;
	}
	public void close(){
		mongo.close();
	}
	@Override
	public void connect() {
		// TODO Auto-generated method stub
	}
}

