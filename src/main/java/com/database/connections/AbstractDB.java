package com.database.connections;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public abstract class AbstractDB {
	final Logger logger = Logger.getLogger(this.getClass());
	String propertiesFilePath="META-INF/configuration.properties";
	Properties connectionProperties = new Properties();
	
	public AbstractDB(){
		logger.debug("Initializing "+this.getClass().getName()+".");
		try {
			this.connectionProperties.load(this.getClass().getClassLoader().getResourceAsStream(this.propertiesFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public abstract void connect();
}
