package com.database.connections;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQL extends AbstractDB {
	//private String url,dbName,user,password,driver;
	Properties connectionProperties = new Properties();
	public static MySQL mysql = new MySQL();
	private MySQL(){
		super();
		try {
			connectionProperties.load(this.getClass().getClassLoader().getResourceAsStream(propertiesFilePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("Couldn't load properties file: "+propertiesFilePath);
			e.printStackTrace();
		}
		try {
			Class.forName(connectionProperties.getProperty("mysql_db_driver")).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			logger.error("Couldn't register driver: "+connectionProperties.getProperty("mysql_db_driver"));
			e.printStackTrace();
		}
		logger.info("Connection to MySQL database initialized.");
	}
	public static MySQL getInstance(){
		return mysql;
	}
	public Connection getConnection() throws SQLException{
		return DriverManager.getConnection(
					connectionProperties.getProperty("mysql_db_protocol")+
					connectionProperties.getProperty("mysql_db_url")+
					":"+
					connectionProperties.getProperty("mysql_db_port")+
					"/"+
					connectionProperties.getProperty("mysql_db_name")+
					connectionProperties.getProperty("mysql_db_create"),
					connectionProperties.getProperty("mysql_db_user"),
					connectionProperties.getProperty("mysql_db_password"));
	}
	public boolean close() throws UnsupportedOperationException{
		throw new UnsupportedOperationException();
//		return false;
	}
	@Override
	public void connect() {
		// TODO Auto-generated method stub
		
	}
}
