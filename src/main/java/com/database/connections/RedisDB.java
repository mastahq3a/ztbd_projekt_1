package com.database.connections;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;

public class RedisDB extends Jedis {
	
	public final static Logger logger = Logger.getLogger(RedisDB.class);
	private Properties connectionProperties = new Properties();
	
	public RedisDB(){
		super();
		logger.debug("Initializing Redis connection and properties.");
		try {
			connectionProperties.load(this.getClass().getClassLoader().getResourceAsStream("META-INF/configuration.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Jedis jedis = new Jedis(connectionProperties.getProperty("redis_db_url"),Integer.valueOf(connectionProperties.getProperty("redis_db_port")));
		//jedis.auth(connectionProperties.getProperty("redis_db_password")); //throws an exception on my environment because password was not set.
		jedis.close();
		this.client.setHost(connectionProperties.getProperty("redis_db_url"));
		this.connect();
		logger.info("Connection to Redis database initialized.");
	}
	
	
}
