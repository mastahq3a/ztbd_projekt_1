package com.data.generators;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import redis.clients.jedis.Jedis;

public class GeneratorProduct {
	Random rand = new Random();
	MongoClient mongo = new MongoClient();
	MongoDatabase mongoDb = mongo.getDatabase("shop");
	MongoCollection<Document> col = mongoDb.getCollection("product");
	MysqlInsert mysql = new MysqlInsert();
	Connection myCon = mysql.getConnection();
	Statement stm;
	Jedis redis = new Jedis();
	Map<String,Integer> myCat = null;
	//price, category_id
	//name, description
	//created_at,updated_at
	//5-telefon,6-laptop,7,monitor,8-słuchawki,9-klawa (dla mysqla)
	String[] companies={"Lenovo","Logiteh","Dell",
			"Samsung","Acer","Asus","LG","Razer","Alienware",
			"Creative","Sony","Microsoft",""};
	
	String[] descriptionS={"Niezwykłe","Zadziwiające","Niepospolite",
			"Nadzwyczajne","Idealne","Cudowne",
			"Nietuzinkowe","Wyszukane","Fenomenalne", "Godne podziwu",
			"Bezkonkurencyjne","Niezrównane","Pierwszorzędne","Innowacyjne"};
	public GeneratorProduct() throws SQLException {
		myCat = this.getCategorysMysql();
	}
	
	public static void main(String[] args) {
		try {
			GeneratorProduct gp = new GeneratorProduct();
			gp.prep();
			gp.addProduct();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
	}
	public void inputRedis(String[] prod){
		String key="product_"+(new Date().getTime());
		Map<String,String> pro = new HashMap<>();
		pro.put("price", prod[0]);
		pro.put("category", prod[1]);
		pro.put("name", prod[2]);
		pro.put("description", prod[3]);
		pro.put("created_at", prod[4]);
		pro.put("updated_at", prod[5]);
		redis.hmset(key, pro);
		redis.sadd("keys", key);
	}
	public void inputMongo(String[] prod){
		Document userDoc = new Document();
		userDoc.append("price", prod[0]);
		userDoc.append("catgory", prod[1]);
		userDoc.append("name", prod[2]);
		userDoc.append("description", prod[3]);
		userDoc.append("created_at", prod[4]);
		userDoc.append("updated_at", prod[5]);
		col.insertOne(userDoc);
	}
	public void inputMysql(String[] prod) throws SQLException{
		stm.executeUpdate("INSERT INTO `product` (`id`,`price`,`category_id`,`name`,`description`,`created_at`,`updated_at`) VALUES "
				+ "(NULL,'"+Float.valueOf(prod[0])+"','"+myCat.get(prod[1])+"','"+prod[2]+"','"+prod[3]+"',NOW(),NOW()) ;");
	}
	public void prep() throws SQLException{
		stm = this.myCon.createStatement();
	}
	public String[] generateProduct(String category){
		String[] prod = new String[6];
		prod[0] = String.valueOf(this.priceGenerator(category));
		prod[1] = category;
		prod[2] = this.productNameGenerator(category);
		prod[3] = this.descGenerator();
		prod[4] = new Date().toString();
		prod[5] = new Date().toString();
		
		return prod;
	}
	
	public void addProduct() throws SQLException{
		for(int i=0;i<300000;i++){
			String[] pro = this.generateProduct("laptop");
			this.inputMongo(pro);
			this.inputRedis(pro);
			this.inputMysql(pro);
		}
		for(int i=0;i<200000;i++){
			String[] pro = this.generateProduct("monitor");
			this.inputMongo(pro);
			this.inputRedis(pro);
			this.inputMysql(pro);
		}
		for(int i=0;i<200000;i++){
			String[] pro = this.generateProduct("telefon");
			this.inputMongo(pro);
			this.inputRedis(pro);
			this.inputMysql(pro);
		}
		for(int i=0;i<150000;i++){
			String[] pro = this.generateProduct("słuchawki");
			this.inputMongo(pro);
			this.inputRedis(pro);
			this.inputMysql(pro);
		}
		for(int i=0;i<150000;i++){
			String[] pro = this.generateProduct("klawiatura");
			this.inputMongo(pro);
			this.inputRedis(pro);
			this.inputMysql(pro);
		}
		
	}
	
	public String productNameGenerator(String category){
		StringBuilder sb = new StringBuilder();
		String company=companies[rand.nextInt(companies.length-1)];
		sb.append(company);
		sb.append(" "+category.toUpperCase().substring(0, 1));
		sb.append((rand.nextInt(99)+10)+company.toUpperCase().substring(0,1)+rand.nextInt(30)+10);
		return sb.toString();
	}
	public Map<String,Integer> getCategorysMysql() throws SQLException{
		Map<String,Integer> ret = new HashMap<>();
		stm = myCon.createStatement();
		ResultSet rs = stm.executeQuery("select name,id FROM category;");
		while(rs.next()){
			ret.put(rs.getString(1), rs.getInt(2));
		}
		return ret;
	}
	public String descGenerator(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.descriptionS[rand.nextInt(descriptionS.length-1)]);
		sb.append(" urządzenie");
		return sb.toString();
	}
	public float priceGenerator(String category){
		if(category.equals("telefon")){
			return round((rand.nextFloat()+0.1f)*1500+600,2);
		}
		if(category.equals("laptop")){
			return round((rand.nextFloat()+0.1f)*5000+1500,2);
		}
		if(category.equals("monitor")){
			return round((rand.nextFloat()+0.1f)*600+300,2);
		}
		if(category.equals("słuchawki")){
			return round((rand.nextFloat()+0.1f)*100+20,2);
		}
		if(category.equals("klawiatura")){
			return round((rand.nextFloat()+0.1f)*100+25,2);
		}
		
		return 0.0f;
	}
	public static float round(float value,int places) {
		if (places < 0) {
			throw new IllegalArgumentException();
		}
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_DOWN);
		return bd.floatValue();
	}
}
