package com.data.generators;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import redis.clients.jedis.Jedis;

public class CategoryAdder {
	private MongoClient mongo = new MongoClient();
	private MongoDatabase db = mongo.getDatabase("shop");
	private MongoCollection<Document> col = db.getCollection("category");
	MysqlInsert mysql = new MysqlInsert();
	Connection myCon = mysql.getConnection();
	Statement stm;
	Jedis redis = new Jedis();
	
	String[] category={"telefon","laptop","monitor","słuchawki","klawiatura"};
	public static void main(String[] args) throws SQLException {
		CategoryAdder ca = new CategoryAdder();
		ca.insertCategories();
	}
	
	public void insertCategories() throws SQLException{
		stm = myCon.createStatement();
		for(String s: category){
			col.insertOne(new Document("name",s));
			Date d = new Date();
			redis.set("category_"+d.getTime(), s);
			stm.executeUpdate("INSERT INTO `category` (`id`,`name`) VALUES (NULL,'"+s+"');");
		}
		stm.close();
		myCon.close();
	}
	
}
