package com.data.generators;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.utils.TimeCounter;

import redis.clients.jedis.Jedis;

public class GeneratorUser {
	Random rand = new Random();
	File nf = new File("names.txt");
	File sf = new File("surnames.txt");
	File cf = new File("cities.txt");
	File stf = new File("streets.txt");
	MongoClient mongo = new MongoClient();
	MongoDatabase mongoDb = mongo.getDatabase("shop");
	MongoCollection<Document> col = mongoDb.getCollection("user");
//	MysqlInsert mysql = new MysqlInsert();
//	Connection myCon = mysql.getConnection();
//	Statement stm;
	Jedis redis = new Jedis();
	BufferedReader names,surnames,cities,streets;
	public static void main(String[] args) throws IOException {
		GeneratorUser ug = new GeneratorUser();
		try{
			ug.openBuffers();
			ug.addUsers();
			ug.closeBuffers(); 	
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void addUsers() throws IOException, SQLException{
		List<String[]> users = new ArrayList<>();
		for(int i=0;i<1000000;i++){
			String[] us = this.generateUser();
			users.add(us);
		}
		TimeCounter.getTimeCounter().startCounting();
		for(int i=0;i<1000000;i++){
			Document userDoc = new Document();
			String[] us = users.get(i);
			userDoc.append("name", us[0]);
			userDoc.append("surname", us[1]);
			userDoc.append("address", us[2]);
			userDoc.append("phone", us[3]);
			col.insertOne(userDoc);
		}
		TimeCounter.getTimeCounter().stopCounting();
		System.out.println("MongoDB: insert - time: "+TimeCounter.getTimeCounter().getTime());
		TimeCounter.getTimeCounter().startCounting();
		for(int i=0;i<1000000;i++){
			String[] us = users.get(i);
			Map<String,String> userData = new HashMap<>();
			userData.put("name", us[0]);
			userData.put("surname", us[1]);
			userData.put("address", us[2]);
			userData.put("phone", us[3]);
			String key = "user_"+(new Date().getTime());
			redis.hmset(key, userData);
			redis.sadd("keys", key);
		}
		TimeCounter.getTimeCounter().stopCounting();
		System.out.println("Redis: insert - time: "+TimeCounter.getTimeCounter().getTime());
//		TimeCounter.getTimeCounter().startCounting();
//		for(int i=0;i<1000000;i++){
//			PreparedStatement pr = this.myCon.prepareStatement("INSERT INTO `user`  (`id`,`name`,`surname`,`address`,`phone`) VALUES "
//					+ "(NULL,?,?,?,?);");
//			String[] us = users.get(i);
//			pr.setString(1, us[0]);
//			pr.setString(2, us[1]);
//			pr.setString(3, us[2]);
//			pr.setString(4, us[3]);
//			pr.executeUpdate();
//			
//			//INSERT INTO `user` (`id`, `name`, `surname`, `address`, `phone`) 
//			//VALUES (NULL, 'asdasd', 'asdasd', 'asdasd', '1231231231');
//		}
//		TimeCounter.getTimeCounter().stopCounting();
//		System.out.println("Mysql: insert - time: "+TimeCounter.getTimeCounter().getTime());
		
		
	}
	public String[] generateUser() throws IOException{
		String[] userData = new String[4];
		if(!names.ready())
			names = new BufferedReader(new FileReader(nf));
		if(!surnames.ready())
			surnames = new BufferedReader(new FileReader(sf));
		if(!cities.ready())
			cities = new BufferedReader(new FileReader(cf));
		if(!streets.ready())
			streets = new BufferedReader(new FileReader(stf));
		
		userData[0]=names.readLine().trim();
		userData[1]=surnames.readLine().trim();
		userData[2]=streets.readLine().trim()+" "
						+(rand.nextInt(250)+1)+" "
						+postalCodeGenerator()+" "+cities.readLine().trim();
		userData[3]=phoneNumberGenerator();
		return userData;
	}
	
	public void openBuffers(){
		try {
			names = new BufferedReader(new FileReader(nf));
			surnames = new BufferedReader(new FileReader(sf));
			cities = new BufferedReader(new FileReader(cf));
			streets = new BufferedReader(new FileReader(stf));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public void closeBuffers(){
		try {
			names.close();
			surnames.close();
			cities.close();
			streets.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String postalCodeGenerator(){
		StringBuilder sb = new StringBuilder();
		sb.append(rand.nextInt(89)+10);
		sb.append("-");
		sb.append(rand.nextInt(899)+100);
		return sb.toString();
	}
	
	public String phoneNumberGenerator(){
		StringBuilder sb = new StringBuilder();
		sb.append("+48");
		sb.append(rand.nextInt(90000000)+100000000);
		return sb.toString();
	}
}
