package com.jobs;

public enum Databases {
	REDIS("redis"),MONGODB("mongodb"),MYSQL("mysql"),
	RIAK("riak"),ORACLE("oracle"),MSSQL("mssql"),
	POSTGRESQL("postgresql"),COUCHDB("couchdb"),
	CASSANDRA("cassandra"),ORIENTDB("orientdb"),
	SQLITE("sqlite"),NEO4J("neo4j"),DASHDB("dashdb"),
	ELASTICSEARCH("elasticsearch"),HBASE("hbase"),
	SAPHANA("saphana"),CLOUDANT("cloudant"),DERBY("derby"),
	ROCKSDB("rocksdb");
	private String dbName;
	private Databases(String dbName){
		this.dbName=dbName;
	}
	public String getDatabaseName(){
		return this.dbName;
	}
	
}
