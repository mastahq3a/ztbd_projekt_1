package com.jobs;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.database.entities.TestResultEntity;
import com.google.gson.Gson;
import com.jezhumble.javasysmon.CpuTimes;
import com.jezhumble.javasysmon.JavaSysMon;
import com.utils.TimeCounter;

abstract class AbstractJob {
	int dbId=-1;
	public static String CONNECTION_URL="http://localhost:8080/ztbd.first/";
	private JavaSysMon jsm;
	final Logger logger = Logger.getLogger(this.getClass());
	long tId = Thread.currentThread().getId();
	String jobName;
	private int jobHits = 100000;
	public Map<String,TestResultEntity> results = new HashMap<>();
	long[] times=new long[4];
	long[] memory=new long[4];
	float[] cpuUsage = new float[4];
	CpuTimes[] cpu= new CpuTimes[4];
	Map<String,TimeCounter> counters = new HashMap<>();
	public AbstractJob(){
			counters.put("create", TimeCounter.getNonStaticTimeCounter("create"));
			counters.put("read", TimeCounter.getNonStaticTimeCounter("read"));
			counters.put("update", TimeCounter.getNonStaticTimeCounter("update"));
			counters.put("delete", TimeCounter.getNonStaticTimeCounter("delete"));
			results.put("create", new TestResultEntity());
			results.put("read", new TestResultEntity());
			results.put("update", new TestResultEntity());
			results.put("delete", new TestResultEntity());
	}
	
	public void run(){
		DbOperations operate = new DbOperations();
		try {
			registerDatabase(operate);
		} catch (UnsupportedDataBaseException e) {
			e.printStackTrace();
			return;
		}
		prepare();
		jsm = new JavaSysMon();
		System.out.println("Hits for every CRUD operation: "+this.jobHits);
		
		System.out.println("Before create");
		cpu[0]=jsm.cpuTimes();
		memory[0] =jsm.physical().getFreeBytes();
		counters.get("create").startCounting();
		for(int i=0;i<this.jobHits;i++){
			create(i);
		}
		counters.get("create").stopCounting();
		memory[0] -=jsm.physical().getFreeBytes();
		cpuUsage[0]=jsm.cpuTimes().getCpuUsage(cpu[0]);
		System.out.println("Create time:"+((times[0]=counters.get("create").getTime())/1000.0));
		
		System.out.println("Before read");
		cpu[1]=jsm.cpuTimes();
		memory[1] =jsm.physical().getFreeBytes();
		counters.get("read").startCounting();
		for(int i=0;i<this.jobHits;i++){
			read(i);
		}
		counters.get("read").stopCounting();
		memory[1] -=jsm.physical().getFreeBytes();
		cpuUsage[1]=jsm.cpuTimes().getCpuUsage(cpu[1]);
		System.out.println("Read time: "+((times[1]=counters.get("read").getTime())/1000.0));
		
		System.out.println("Before update");
		cpu[2]=jsm.cpuTimes();
		memory[2] =jsm.physical().getFreeBytes();
		counters.get("update").startCounting();
		for(int i=0;i<this.jobHits;i++){
			update(i);
		}
		counters.get("update").stopCounting();
		memory[2] -=jsm.physical().getFreeBytes();
		cpuUsage[2]=jsm.cpuTimes().getCpuUsage(cpu[2]);
		System.out.println("Update time: "+((times[2]=counters.get("update").getTime())/1000.0));
		
		System.out.println("Before delete");
		cpu[3]=jsm.cpuTimes();
		memory[3] =jsm.physical().getFreeBytes();
		counters.get("delete").startCounting();
		for(int i=0;i<this.jobHits;i++){
			delete(i);
		}
		counters.get("delete").stopCounting();
		memory[3] -=jsm.physical().getFreeBytes();
		cpuUsage[3]=jsm.cpuTimes().getCpuUsage(cpu[3]);
		System.out.println("Delete time: "+((times[3]=counters.get("delete").getTime())/1000.0));
		System.out.println("------------------------------------------");
		System.out.println("Times for CRUD operations: ");
		for(Map.Entry<String,TimeCounter> ent : counters.entrySet()){
			System.out.println(ent.getKey()+": "+(ent.getValue().getTime()/1000.0));
		}
		System.out.println(Arrays.toString(this.times));
		sendResults();
	}
	private void sendResults(){
		int resultsIterator=0;
		for(Map.Entry<String, TestResultEntity> ent : results.entrySet()){
			TestResultEntity c = ent.getValue();
			c.setHits(this.jobHits);
			c.setTime(times[resultsIterator]);
			c.setMemory(memory[resultsIterator]);
			c.setCpu(cpuUsage[resultsIterator]);
			c.setJobName(ent.getKey());
			resultsIterator++;
		}
		String resultsPacked =new Gson().toJson(results); 
		try {
			URL url = new URL(AbstractJob.CONNECTION_URL+"test/send/result/"+dbId);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			//conn.setRequestProperty("Content-Type", "application/json");
			OutputStream os = conn.getOutputStream();
			os.write(resultsPacked.getBytes());
			os.flush();
			if(conn.getResponseCode()!= HttpURLConnection.HTTP_CREATED){
				System.err.println("Couldn't send results to the server."
						+"\nHTTP error code: "+conn.getResponseCode());
				return;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			System.out.println("SERVER: "+br.readLine());
			br.close();
			conn.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	protected void setTenThousandHits(){
		this.jobHits=10000;
	}
	protected void setHundredThousandHits(){
		this.jobHits=100000;
	}
	protected void setMilionHits(){
		this.jobHits=1000000;
	}
	public String getJobName(){
		return this.jobName;
	}
	public int getJobHits(){
		return this.jobHits;
	}
	public void setJobName(String jobName){
		this.jobName=jobName;
	}
	public long[] getTimes(){
		return this.times;
	}
	public abstract void registerDatabase(DbOperations operate) throws UnsupportedDataBaseException;
	public abstract void prepare();
	public abstract void create(int index);
	public abstract void read(int index);
	public abstract void update(int index);
	public abstract void delete(int index);
	public class DbOperations{
		public void regiesterDatabase(Databases name){
			try {
				URL registerUrl = new URL(CONNECTION_URL+"register/"+name.getDatabaseName());
				InputStream resp = registerUrl.openStream();
				DataInputStream dis = new DataInputStream(new BufferedInputStream(resp));
				String r = dis.readLine();
				dbId = Integer.valueOf(r.trim());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
			System.out.println("Database id: "+dbId+", database name: "+name.getDatabaseName());
		}
		public void setTenThousandHits(){
			AbstractJob.this.setTenThousandHits();
		}
		public void setHundredThousandHits(){
			AbstractJob.this.setHundredThousandHits();
		}
		public void setMilionHits(){
			AbstractJob.this.setMilionHits();
		}
	}
}
