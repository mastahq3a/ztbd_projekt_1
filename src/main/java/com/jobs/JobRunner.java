package com.jobs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.jezhumble.javasysmon.JavaSysMon;

public class JobRunner {
	private JavaSysMon jsm;
	private Map<String,AbstractJob> jobs = null;
	private Map<String,Long> jobTimes = null;
	private long executionTime; //in seconds
	private long usedMem;
	public JobRunner(){
		this.jobs = new HashMap<String,AbstractJob>();
	}
	public JobRunner addJob(AbstractJob job){
		this.jobs.put(job.getJobName(),job);
		return this;
	}
	
	public void run(){
		executionTime=-1;
		jobTimes = new HashMap<String,Long>();
		//executionTime=0;
		usedMem=0;
		jsm = new JavaSysMon();
		long jobStart=0,jobEnd=0;
		long startTime=System.currentTimeMillis();
		long freeMem = jsm.physical().getFreeBytes();
		for(AbstractJob job : jobs.values()){
			jobStart=System.currentTimeMillis();
			job.run();
			jobEnd=System.currentTimeMillis();
			jobTimes.put(job.getJobName(),(jobStart-jobEnd));
		}
		this.usedMem=freeMem-jsm.physical().getFreeBytes();
		long endTime=System.currentTimeMillis();
		this.executionTime=(endTime-startTime);
	}
	public void run(String jobName){
		executionTime=-1;
		jobTimes = new HashMap<String,Long>();
		//executionTime=0;
		usedMem=0;
		AbstractJob job = jobs.get(jobName);
		jsm = new JavaSysMon();
		long jobStart=0,jobEnd=0;
		long startTime=System.currentTimeMillis();
		long freeMem = jsm.physical().getFreeBytes();
		jobStart=System.currentTimeMillis();
		job.run();
		jobEnd=System.currentTimeMillis();
		jobTimes.put(job.getJobName(),(jobStart-jobEnd));
		this.usedMem=freeMem-jsm.physical().getFreeBytes();
		long endTime=System.currentTimeMillis();
		this.executionTime=(endTime-startTime);
	}
	public long getExecutionTime(){
		return this.executionTime;
	}
	public void deleteJob(){
		int size = this.jobs.size();
		if(size>0){
			this.jobs.remove(size-1);
		}
	}
	public List<AbstractJob> getJobs(){
		return this.getJobs();
	}
	public long getUsedMem(){
		return this.usedMem;
	}
	public Map<String,Long> getJobTimes(){
		return this.jobTimes;
	}
	
}
