import {Injectable} from "@angular/core";
import {Subject} from "rxjs/Rx";

@Injectable()
export class NotificationService {
  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();

  public notifyOther(data: any) {
    this.notify.next(data);
  }
}
