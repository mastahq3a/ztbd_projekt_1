import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Headers, Response} from "@angular/http";
import 'rxjs/add/operator/map'
import {Variables} from "../variables";
import {StorageService} from "./storage.service";

@Injectable()
export class CategoryService {
  constructor(private http: Http, private storageService: StorageService) {

  }

  private basePath: string = Variables.BASE_API_URL + 'categories?dbName=';

  get(options: any): Observable<any> {
    return this.http.get(this.basePath + this.storageService.get('database') + '&page=' + options.page + '&cursor=' + options.cursor + '&limit=' + options.limit)
      .map((response: Response) => {
        return response.json();
      })
  }

  post(data: any): Observable<any> {
    return this.http.post(this.basePath + this.storageService.get('database'), data)
      .map((response: Response) => {
        return response.json();
      })
  }

  remove(data: any): Observable<any> {
    return this.http.delete(this.basePath + this.storageService.get('database') + '&id=' + data.id)
      .map((response: Response) => {
        return response.json();
      })
  }
}
