import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Headers, Response} from "@angular/http";
import 'rxjs/add/operator/map'

@Injectable()
export class DataService {
  constructor(private http: Http) {

  }

  get(data: any): Observable<boolean> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get('test/avg/name/' + data.database1 + '/' + data.database2 + '/' + data.operation, {headers})
      .map((response: Response) => {
        return response.json();
      })
  }
}
