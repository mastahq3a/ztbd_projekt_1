import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {
  private data: any = {};

  get(key: string): any {
    if (!this.data.hasOwnProperty(key)) {
      return null;
    }
    return this.data[key];
  }

  set(key: string, value: any) {
    this.data[key] = value;
  }
}
