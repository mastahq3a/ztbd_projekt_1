import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Headers, Response} from "@angular/http";
import 'rxjs/add/operator/map'
import {Variables} from "../variables";
import {StorageService} from "./storage.service";

@Injectable()
export class TestService {
  constructor(private http: Http, private storageService: StorageService) {

  }

  private basePath: string = Variables.BASE_API_URL + 'tests?dbName=';

  get(options: any): Observable<any> {
    return this.http.get(this.basePath + this.storageService.get('database'))
      .map((response: Response) => {
        return response.json();
      })
  }
}
