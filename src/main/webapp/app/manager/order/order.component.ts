import {Component, OnInit, OnDestroy} from '@angular/core';
import {OrderService} from "../../service/order.service";
import {Subscription} from "rxjs";
import {NotificationService} from "../../service/notification.service";

@Component({
  selector: 'order',
  templateUrl: 'app/manager/order/order.component.html',
})

export class OrderComponent implements OnInit, OnDestroy {
  private orders: any;
  private error: any;
  private info: string = '';
  private subscription: Subscription;
  private itemsPerPage: number = 50;
  private totalItems: number = 100;
  private currentPage: number = 1;
  private prevCursor: number = 0;
  private cursor: number = 0;

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.get();
  }

  constructor(private orderService: OrderService, private notificationService: NotificationService) {

  }

  ngOnInit(): void {
    this.subscription = this.notificationService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('database')) {
        this.get();
      }
    });
    this.get();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  get() {
    this.orderService.get({
      page: this.currentPage,
      cursor: this.cursor,
      limit: this.itemsPerPage
    })
      .subscribe(
        data => {
          this.orders = data.orders;
          this.info = data.stats;
          this.error = '';
          if (data.stats.hasOwnProperty('cursor')) {
            this.prevCursor = this.cursor;
            this.cursor = data.stats.cursor;
          }
        },
        error => {
          this.error = error;
        }
      )
  }

  post() {
    this.orderService.post({})
      .subscribe(data => {
          this.orders.push(data.order);
          this.info = data.stats;
          this.error = '';
        },
        error => {
          this.error = error;
        });
  }

  remove(order: any) {
    let id = order.id || order._id;
    this.orderService.remove({
      id: id
    }).subscribe(data => {
        for (let o in this.orders) {
          if (this.orders[o].id === id || this.orders[o]._id === id) {
            this.orders.splice(o, 1);
          }
        }
        this.info = data.stats;
        this.error = '';
      },
      error => {
        this.error = error;
      });
  }
}
