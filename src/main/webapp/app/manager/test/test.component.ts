import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {NotificationService} from "../../service/notification.service";
import {TestService} from "../../service/test.service";
import {StorageService} from "../../service/storage.service";

@Component({
  selector: 'test',
  templateUrl: 'app/manager/test/test.component.html',
})

export class TestComponent implements OnInit, OnDestroy {
  private error: any;
  private info: string = '';
  private subscription: Subscription;
  private database: string = 'mysql';

  constructor(private testService: TestService, private notificationService: NotificationService, private storageService: StorageService) {
  }

  ngOnInit(): void {
    this.subscription = this.notificationService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('database')) {
        this.database = res.database;
      }
    });
    this.database = this.storageService.get('database');
  }

  get() {
    this.testService.get({})
      .subscribe(
        data => {
          this.info = data.stats;
          this.error = '';
        },
        error => {
          this.error = error;
        }
      )
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
