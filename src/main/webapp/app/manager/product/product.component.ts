import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {Subscription} from "rxjs";
import {NotificationService} from "../../service/notification.service";

@Component({
  selector: 'product',
  templateUrl: 'app/manager/product/product.component.html',
})

export class ProductComponent implements OnInit, OnDestroy {
  private products: any;
  private error: any;
  private info: string = '';
  private subscription: Subscription;
  private itemsPerPage: number = 50;
  private totalItems: number = 100;
  private currentPage: number = 1;
  private prevCursor: number = 0;
  private cursor: number = 0;

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.get();
  }

  constructor(private productService: ProductService, private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.subscription = this.notificationService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('database')) {
        this.get();
      }
    });
    this.get();
  }

  get() {
    this.productService.get({
      page: this.currentPage,
      cursor: this.cursor,
      limit: this.itemsPerPage
    })
      .subscribe(
        data => {
          this.products = data.products;
          this.info = data.stats;
          this.error = '';
          if (data.stats.hasOwnProperty('cursor')) {
            this.prevCursor = this.cursor;
            this.cursor = data.stats.cursor;
          }
        },
        error => {
          this.error = error;
        }
      )
  }

  post() {
    let timestamp = new Date().getTime();
    this.productService.post({})
      .subscribe(data => {
          this.products.push(data.product);
          this.info = data.stats;
          this.error = '';
        },
        error => {
          this.error = error;
        });
  }

  remove(product: any) {
    let id = product.id || product._id;
    this.productService.remove({
      id: id
    }).subscribe(data => {
        for (let p in this.products) {
          if (this.products[p].id === id || this.products[p]._id === id) {
            this.products.splice(p, 1);
          }
        }
        this.info = data.stats;
        this.error = '';
      },
      error => {
        this.error = error;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
