import {Component, OnInit} from '@angular/core';
import {DataService} from "../service/data.service";
import {NotificationService} from "../service/notification.service";
import {StorageService} from "../service/storage.service";

@Component({
  selector: 'manager',
  templateUrl: 'app/manager/manager.component.html',
})

export class ManagerComponent implements OnInit {
  constructor(private dataService: DataService, private notificationService: NotificationService, private storageService: StorageService) {
  }

  private model: any = {
    database: 'mysql'
  };
  private error: string = '';
  private results: any = {};

  ngOnInit(): void {
    this.changeDatabase();
  }

  private changeDatabase() {
    this.storageService.set("database", this.model.database);
    this.notificationService.notifyOther({
      database: this.model.database
    });
  }
}
