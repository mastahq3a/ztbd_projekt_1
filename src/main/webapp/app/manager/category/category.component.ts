import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {Subscription} from "rxjs";
import {NotificationService} from "../../service/notification.service";
import {CategoryService} from "../../service/category.service";

@Component({
  selector: 'category',
  templateUrl: 'app/manager/category/category.component.html',
})

export class CategoryComponent implements OnInit, OnDestroy {
  private categories: any;
  private error: any;
  private info: string = '';
  private subscription: Subscription;
  private itemsPerPage: number = 50;
  private totalItems: number = 100;
  private currentPage: number = 1;
  private prevCursor: number = 0;
  private cursor: number = 0;

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.get();
  }

  constructor(private categoryService: CategoryService, private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.subscription = this.notificationService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('database')) {
        this.get();
      }
    });
    this.get();
  }

  get() {
    this.categoryService.get({
      page: this.currentPage,
      cursor: this.cursor,
      limit: this.itemsPerPage
    })
      .subscribe(
        data => {
          this.categories = data.categories;
          this.info = data.stats;
          this.error = '';
          if (data.stats.hasOwnProperty('cursor')) {
            this.prevCursor = this.cursor;
            this.cursor = data.stats.cursor;
          }
        },
        error => {
          this.error = error;
        }
      )
  }

  post() {
    let timestamp = new Date().getTime();
    this.categoryService.post({})
      .subscribe(data => {
          this.categories.push(data.category);
          this.info = data.stats;
          this.error = '';
        },
        error => {
          this.error = error;
        });
  }

  remove(category: any) {
    let id = category.id || category._id;
    this.categoryService.remove({
      id: id
    }).subscribe(data => {
        for (let c in this.categories) {
          if (this.categories[c].id === id || this.categories[c]._id === id) {
            this.categories.splice(c, 1);
          }
        }
        this.info = data.stats;
        this.error = '';
      },
      error => {
        this.error = error;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
