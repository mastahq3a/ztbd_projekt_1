import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {NotificationService} from "../../service/notification.service";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'user',
  templateUrl: 'app/manager/user/user.component.html',
})

export class UserComponent implements OnInit, OnDestroy {
  private users: any;
  private error: any;
  private info: string = '';
  private subscription: Subscription;
  private itemsPerPage: number = 50;
  private totalItems: number = 100;
  private currentPage: number = 1;
  private prevCursor: number = 0;
  private cursor: number = 0;

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.get();
  }

  constructor(private userService: UserService, private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.subscription = this.notificationService.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('database')) {
        this.get();
      }
    });
    this.get();
  }

  get() {
    this.userService.get({
      page: this.currentPage,
      cursor: this.cursor,
      limit: this.itemsPerPage
    })
      .subscribe(
        data => {
          this.users = data.users;
          this.info = data.stats;
          this.error = '';
          if (data.stats.hasOwnProperty('cursor')) {
            this.prevCursor = this.cursor;
            this.cursor = data.stats.cursor;
          }
        },
        error => {
          this.error = error;
        }
      )
  }

  post() {
    let timestamp = new Date().getTime();
    this.userService.post({})
      .subscribe(data => {
          this.users.push(data.user);
          this.info = data.stats;
          this.error = '';
        },
        error => {
          this.error = error;
        });
  }

  remove(user: any) {
    let id = user.id || user._id;
    this.userService.remove({
      id: id
    }).subscribe(data => {
        for (let u in this.users) {
          if (this.users[u].id === id || this.users[u]._id === id) {
            this.users.splice(u, 1);
          }
        }
        this.info = data.stats;
        this.error = '';
      },
      error => {
        this.error = error;
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
