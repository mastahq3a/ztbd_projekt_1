import {NgModule, Injectable}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent}  from './app.component';
import {FormsModule}    from '@angular/forms';
import {ChartsModule} from 'ng2-charts/ng2-charts';
import {HttpModule} from "@angular/http";
import {DataService} from "./service/data.service";
import {NavbarComponent} from "./navbar/navbar.component";
import {RouterModule} from "@angular/router";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ManagerComponent} from "./manager/manager.component";
import {ProductComponent} from "./manager/product/product.component";
import {CategoryComponent} from "./manager/category/category.component";
import {OrderComponent} from "./manager/order/order.component";
import {UserComponent} from "./manager/user/user.component";
import {ProductService} from "./service/product.service";
import {CategoryService} from "./service/category.service";
import {UserService} from "./service/user.service";
import {OrderService} from "./service/order.service";
import {NotificationService} from "./service/notification.service";
import {PaginationModule} from 'ng2-bootstrap';
import {StorageService} from "./service/storage.service";
import {TestService} from "./service/test.service";
import {TestComponent} from "./manager/test/test.component";

@NgModule({
  imports: [BrowserModule, FormsModule, ChartsModule, HttpModule, PaginationModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'manager',
        component: ManagerComponent,
        children: [
          {
            path: 'products',
            component: ProductComponent
          },
          {
            path: 'categories',
            component: CategoryComponent
          },
          {
            path: 'users',
            component: UserComponent
          },
          {
            path: 'orders',
            component: OrderComponent
          },
          {
            path: 'test',
            component: TestComponent
          },
          {
            path: '',
            redirectTo: '/manager/products',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      }
    ])],
  declarations: [
    DashboardComponent,
    AppComponent,
    NavbarComponent,
    ManagerComponent,
    ProductComponent,
    CategoryComponent,
    UserComponent,
    TestComponent,
    OrderComponent
  ],
  bootstrap: [AppComponent],
  providers: [NotificationService, DataService, ProductService, CategoryService, UserService, OrderService, TestService, StorageService]
})

export class AppModule {
}
