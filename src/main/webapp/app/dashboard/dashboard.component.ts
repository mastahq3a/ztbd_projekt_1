import {Component} from '@angular/core';
import {DataService} from "../service/data.service";

@Component({
  selector: 'dashboard',
  templateUrl: 'app/dashboard/dashboard.component.html',
})

export class DashboardComponent {
  constructor(private dataService: DataService) {
  }

  private model: any = {
    database1: 'mongodb',
    database2: 'redis',
    operation: 'create'
  };

  private error: string = '';
  private results: any = {};

  private barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  private barChartType: string = 'bar';
  private barChartLegend: boolean = true;

  private barChartLabels: string[] = [];
  private barChartData: any[] = [];
  private barChartDataCpu: any[] = [];
  private barChartDataRam: any[] = [];

  private updateChart() {
    this.barChartLabels = this.results.records;
    let times1 = this.results.times[this.model.database1];
    let times2 = this.results.times[this.model.database2];
    for (let t1 in times1) {
      times1[t1] = times1[t1] / 1000;
    }
    for (let t2 in times2) {
      times2[t2] = times2[t2] / 1000;
    }
    let cpu1 = this.results.cpu[this.model.database1];
    let cpu2 = this.results.cpu[this.model.database2];
    let memory1 = this.results.memory[this.model.database1];
    let memory2 = this.results.memory[this.model.database2];

    this.barChartData = [
      {
        data: times1, label: this.model.database1,
      },
      {
        data: times2, label: this.model.database2,
      }];

    this.barChartDataCpu = [
      {
        data: cpu1, label: this.model.database1,
      },
      {
        data: cpu2, label: this.model.database2,
      }];

    this.barChartDataRam = [
      {
        data: memory1, label: this.model.database1,
      },
      {
        data: memory2, label: this.model.database2,
      }];
  }

  private compare() {
    this.dataService.get(this.model)
      .subscribe(
        data => {
          this.results = data;
          this.error = '';
          this.updateChart()
        },
        error => {
          this.error = error.json();
        }
      );
  }
}
