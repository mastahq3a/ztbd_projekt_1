package com.tests;

import static org.junit.Assert.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.junit.Test;

import com.database.connections.MongoDB;
import com.database.connections.MySQL;
import com.database.connections.RedisDB;
import com.mongodb.client.MongoDatabase;

public class DatabasesTest {

	@Test
	public void mySQLConnectionTest(){
		MySQL mysql = MySQL.getInstance();
		try {
			assertTrue(mysql.getConnection()!=null);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void redisConnectionTest(){
		try{
			RedisDB redis = new RedisDB();
			assertTrue(redis!=null);
			redis.close();
		}catch(Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void mongoDBConnectionTest(){
		MongoDB mongo = new MongoDB();
		MongoDatabase db = mongo.getDB();
		//mongo.getDB().createCollection("testcollection");
//		for(int i=0;i<1000000;i++){
//			Document doc = new Document("name","MongoDB").append("type"+i, "database"+i);
//			db.getCollection("testcollection").insertOne(doc);
//		}
		List<String> dbs = mongo.getMongoClient().getDatabaseNames();
		System.out.println(Arrays.toString(dbs.toArray()));
		if(!dbs.contains("ztbd")){
			mongo.close();
			fail("No db in Mongo...");
		}
		
		//mongo.getDB().getCollection("test-collection").drop();
		//mongo.getDB().drop();
		mongo.close();
	}
	
	@Test
	public void mySQLOperationsTest(){
		MySQL mysql = MySQL.getInstance();
		java.sql.Connection connection = null;
		try {
			connection = mysql.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
		if(connection!=null){
			try {
				connection.createStatement().executeUpdate("CREATE TABLE test_table (id int not null unique AUTO_INCREMENT, name varchar(255) not null,PRIMARY KEY(id) );");
				java.sql.ResultSet rs = connection.createStatement().executeQuery("show tables;");
				if(rs.isBeforeFirst()){
					List<String> tbNames = new ArrayList<>();
					while(rs.next()){
						tbNames.add(rs.getString(1));
					}
					if(!tbNames.contains("test_table")){
						fail("Table not created!");
					}
				}else{
					fail("Table not created");
				}
				connection.createStatement().executeUpdate("drop table test_table;");
				rs = connection.createStatement().executeQuery("show tables;");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				fail();
			}
		}
	}
	@Test
	public void redisOperationsTest(){
		RedisDB redis = new RedisDB();
		assertTrue(redis.set("test-key",String.valueOf(1)).equals("OK"));
		assertTrue(redis.incr("test-key")==2L);
		assertTrue(redis.get("test-key").equals("2"));
		assertTrue(redis.del("test-key")==1);
		assertTrue(redis.del("test-key").equals(0L));
		assertTrue(redis.get("test-key")==null);
		assertNull(redis.get("test-key"));
		redis.close();
	}
	
}
