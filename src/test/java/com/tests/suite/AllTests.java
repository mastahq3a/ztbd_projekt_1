package com.tests.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.tests.DatabasesTest;
import com.tests.ProcessMonitorTest;

@RunWith(Suite.class)
@SuiteClasses({
	DatabasesTest.class,
	ProcessMonitorTest.class
})
public class AllTests {
	
}
